#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>


typedef struct {
    int numtrain;
    char villedep[30];
    char villearv[30];
    char heuredep[6];
    char heurearv[6];
    int prix1;
    int prix2;
    char reduc[10];
} trains ;



// ChatBox avec le client

void funcChat(int sockfd){
    char buff[200];
    char buff2[200];
    trains tab[30];
    FILE * f;
    char ligne[61];
    char *elem;
    int i = 0;
    int j;    
    //char *buff=malloc(sizeof(char)*200);
    //char *buff2=malloc(sizeof(char)*200);
    int n;

    f = fopen("Trains.txt","r");

    if(f==NULL){
        printf("erreur ouverture du fichier");
    }
    
     while (fgets(ligne,sizeof(ligne),f)!=NULL) {
                //printf("%s",ligne);

        elem = strtok(ligne,";");
        tab[i].numtrain = atoi(elem);

        elem = strtok(NULL,";");
        strcpy(tab[i].villedep,elem);

        elem = strtok(NULL,";");
        strcpy(tab[i].villearv,elem);


        elem = strtok(NULL,";");
        strcpy(tab[i].heuredep,elem);

        elem = strtok(NULL,";");
        strcpy(tab[i].heurearv,elem);


        elem = strtok(NULL,";");
        tab[i].prix1=atoi(elem);

        elem = strtok(NULL,";");
        tab[i].prix2=atoi(elem);

        elem = strtok(NULL,"\n");
        strcpy(tab[i].reduc,elem);

        i++;
        }
        printf("tab0 %s\n",tab[0].villedep );
    fclose(f);
        //bzero(buff, 200);
        //bzero(buff2, 200);
        //lire le message du client et le copier dans le tampon

        read(sockfd, buff, sizeof(buff));
        read(sockfd, buff2, sizeof(buff2));
        send(sockfd, buff, strlen(buff), 0);
        /*
        for(j=0;j<i;j++){
        	if((strcmp(buff,tab[j].villedep)==0) && (strcmp(buff2,tab[j].villearv)==0)){
        			//printf("%d  |  %s  |  %s  |  %s  |  %s  |  %d.%d  |  %s\n",tab[j].numtrain,tab[j].villedep,tab[j].villearv,tab[j].heuredep,tab[j].heurearv,tab[j].prix1,tab[j].prix2,tab[j].reduc);
        	strcpy(buff, tab[j].villedep);
        	bzero(buff, 200);
        	
        	
        		}
        	}
        	*/
        // tampon d'inpression contenant le contenu du client
        printf("From client: %s\n", buff);
        printf("From client: %s\n", buff2);

        bzero(buff, 200);

        // l'envoie vers le client


}

void affiche_aide() {
    printf("Usage: \tserveur <numero port> <nombre max clients>\n");
    printf("\tExemple: serveur 20000 15\n");
    exit(-1);
}


void end_of_service() {
    wait(NULL);
}

void servir_client(int socket_client) {
int n;
char buffer[1020];
printf("\nConnexion etablie avec le socket %d\n\n", socket_client);
}

/** @brief Cree un processus fils qui se chargera de traiter la demande du client
 *  @param int socket_client : Le socket (descripteur de fichier) qu'il faut servir, c'est la socket du client
 *  @param int socket_server : Le socket (descripteur de fichier) qu'il faut fermer, c'est la socket du père
 *  @return void : Pas de valeur de retour
 **/
void creation_process(int socket_client, int socket_server) {
    switch (fork()) {
        case -1:
            perror("fork");
            exit(-1);
        case 0:
            close(socket_server);
            servir_client(socket_client);
            funcChat(socket_client);
            exit(0);
        default:
            //fermer la socket de service
            close(socket_client);
    }
}

int main(int argc, char const *argv[])
{
    //declaration des variables
    int socket_server, socket_client;
    int binded;
    int loop = 1;
    unsigned int client_add_len;
    struct sigaction sign; /* déclaration d'une variable de type struct sigaction */
    struct sockaddr_in server_add, client_add;

    if (argc != 3) {
        affiche_aide();
    }

    const int N_PORT = atoi(argv[1]);
    const int MAX_CLIENTS = atoi(argv[2]);

    sign.sa_handler = end_of_service; /* le champ sa_handler de cette variable reçoit (le nom de) la fonction sur laquellele déroutement devra se faire */
    sign.sa_flags = SA_RESTART; /* cela permettra d'eviter l'interruption de "accept" par la reception du SIGCHLD */
    sigaction(SIGCHLD, &sign, NULL);

    //preparation des champs pour sockaddr_in adresse
    client_add.sin_family = AF_INET;
    client_add.sin_port = htons(N_PORT);
    client_add.sin_addr.s_addr = htonl(INADDR_ANY);



    socket_server = socket(AF_INET, SOCK_STREAM, 0);
    

    system("clear");
    if (socket_server == -1) {
        perror("socketServ");
        exit(-1);
    }

    //attachement de la socket d'ecoute à une adresse
    printf("bind...\n");
    binded = bind(socket_server, (struct sockaddr *) &client_add, sizeof (client_add));
    if (binded == -1) {
        perror("bind");
        exit(-1);
    }

        //ouverture du service sur la socket d'ecoute
    printf("listen...\n");
    if (listen(socket_server, MAX_CLIENTS) == -1) {
        perror("listen");
        exit(-1);
    }



printf("arrive...\n");
    client_add_len = sizeof (server_add);
    while (loop) {
        socket_client = accept(socket_server, (struct sockaddr *) &client_add, &client_add_len);
        if (socket_client == -1) {
            perror("accept");
            exit(-1);
        }
        creation_process(socket_client, socket_server);
    }
    close(socket_server);

    return 0;
}
