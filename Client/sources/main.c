#include "../headers/fonctions.h"

int main(int argc, char const *argv[]){
    struct sockaddr_in server_add = {0}; // adresse du serveur
    struct hostent *infos_server = NULL;
    int socket_client;
    int choix = -1; // temoin pour la lecture avec le buffer
    const char *hostname = "localhost"; // nom du serveur

    if (argc != 3) {
        affiche_aide();
    }

    hostname = argv[1];
    const int N_PORT = atoi(argv[2]);
    system("clear");
    socket_client = socket(AF_INET, SOCK_STREAM, 0); // initialisation du descripteur
    if (socket_client == -1)
    {
        perror("socket_client");
        exit(-1);
    }

    infos_server = gethostbyname(hostname); /* on récupère les informations du serveur */

    if (infos_server == NULL) {
        perror("infos_server");
        exit(-1);
    }

    server_add.sin_family = AF_INET;
    memcpy(&(server_add.sin_addr.s_addr), infos_server->h_addr, sizeof (u_long));
    server_add.sin_port = htons(N_PORT);

    if (connect(socket_client, (struct sockaddr *) &server_add, sizeof (server_add)) == -1) {
        perror("connect");
        exit(-1);
    }

    while (1) {
        choix = -1;
        choix = menu_client();
        char *choi=malloc(sizeof(char)*10);
        sprintf(choi,"%d", choix);
        send(socket_client, choi, sizeof(choi),0);
        if (choix == 3) {
            system("clear");
            printf("Train par horaire \n");
            Affheure(socket_client);
        } else if (choix == 2) {
            system("clear");
            AffVdepvarv(socket_client);
            
        } else if (choix == 1) {
            system("clear");
            printf("Liste des trains disponible\n");
            afficher();
        } else if (choix == 0) {
            close(socket_client);
            exit(EXIT_SUCCESS);
        }
    }


    
    close(socket_client);
    return (EXIT_SUCCESS);
}

