#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <signal.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <setjmp.h>

#include "definitions.h"


void AffVdepvarv(int sockfd);

void Affheure(int sockfd);


/** @brief Permet le chat du client vers le server
 *  @param int socket du client
 *  @return void
 **/
void funcChat(int sockfd);


/**  Affiche l'aide quand l'utilisateur ne sais pas comment tester
 **/
void affiche_aide();



/** @brief Permet la saisie des choix client en controlant les limites
 *  @param char message: message à afficher pour inviter à la saisie
 *  @param int nbr_choix: quantité maximale autorisé de choix
 *  @return int: le choix saisi par le client
 **/
int saisir(char message[], int nbr_choix);

/**
* Juste l'affichage de tous les trains disponible
*/
void afficher();

int menu_client();
