#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#define MAX 80 
#define PORT 8080 
#define SA struct sockaddr 
  
// Fonction conçue pour la discussion entre le client et le serveur. 
void func(int sockfd) 
{ 
    char buff[MAX]; 
    int n; 
    // boucle infinie pour le chat
    for (;;) { 
        bzero(buff, MAX); 
  
        // lire le message du client et le copier dans la mémoire tampon 
        read(sockfd, buff, sizeof(buff)); 
        // tampon d'impression contenant le contenu du client 
        printf("From client: %s\t To client : ", buff); 
        bzero(buff, MAX); 
        n = 0; 
        // copy du message du server dans le buffer 
        while ((buff[n++] = getchar()) != '\n') 
            ; 
  
        // et l'envoyer au client 
        write(sockfd, buff, sizeof(buff)); 
  
        // if msg contains "Exit" then server exit and chat ended. 
        if (strncmp("exit", buff, 4) == 0) { 
            printf("Server Exit...\n"); 
            break; 
        } 
    } 
} 
  
// Driver function 
int main() 
{ 
    int sockfd, connfd, len; 
    struct sockaddr_in servaddr, cli; 
  
    // socket create and verification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("creation du socket échoué ...\n"); 
        exit(0); 
    } 
    else
        printf("creation du socket reçu..\n"); 
    bzero(&servaddr, sizeof(servaddr)); 
  
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    servaddr.sin_port = htons(PORT); 
  
    // Liaison du socket nouvellement créé à une adresse IP donnée et vérification 
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
        printf("liai du socket failed...\n"); 
        exit(0); 
    } 
    else
        printf("Socket lié avec succès..\n"); 
  
    // Now server is ready to listen and verification 
    if ((listen(sockfd, 5)) != 0) { 
        printf("ecoute echoué...\n"); 
        exit(0); 
    } 
    else
        printf("ecoute du serveur..\n"); 
    len = sizeof(cli); 
  
    // Accept the data packet from client and verification 
    connfd = accept(sockfd, (SA*)&cli, &len); 
    if (connfd < 0) { 
        printf("server acccept failed...\n"); 
        exit(0); 
    } 
    else
        printf("server acccept le client...\n"); 
  
    // Function for chatting between client and server 
    func(connfd); 
  
    // After chatting close the socket 
    close(sockfd); 
} 