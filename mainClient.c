#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <signal.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <setjmp.h>
#define SA struct sockaddr

void funcChat(int sockfd){
    char buffDepart[200];
    char buffArrive[200];
    char *buffer[500];

    int n = 0;

        bzero(buffDepart, sizeof(buffDepart));

        printf("Votre ville de départ :\n");
        n = 0;
        while ((buffDepart[n++] = getchar()) != '\n')
            ;
        write(sockfd, buffDepart, sizeof(buffDepart));
        printf("Votre ville d'arrivée: \n");
        n = 0;
        while ((buffArrive[n++] = getchar()) != '\n')
            ;
        write(sockfd, buffArrive, sizeof(buffArrive));
        

        //je lis le message du server réçu
        //read(sockfd, buffDepart, sizeof(buffDepart));
        //read(sockfd, buffArrive, sizeof(buffArrive));

       // recv(sockfd, buffArrive, sizeof(char) * 8, 0);
        printf("From Server : %s", buffArrive);
    
}



/**  Affiche l'aide quand l'utilisateur ne sais pas comment tester
 **/
void affiche_aide() {
    printf("Usage: \nclient <nom du serveur> <numero port>\n");
    printf("\tExemple: client 192.168.1.23 20000\n");
    exit(-1);
}

/** @brief Permet la saisie des choix client en controlant les limites
 *  @param char message: message à afficher pour inviter à la saisie
 *  @param int nbr_choix: quantité maximale autorisé de choix
 *  @return int: le choix saisi par le client
 **/
int saisir(char message[], int nbr_choix) {
    int choix = -1;
    char saisie[255];
    do
    {
        printf("\n--%s \n---> : ", message );
        fgets(saisie, 255, stdin);
        if(sscanf(saisie, "%d", &choix) == 1) break;
        if (choix < 0 || choix > nbr_choix)
        {
            printf("choix non disponible. Merci de choisi rentre 0 et %d", nbr_choix);
        }
    } while (choix < 0 || choix > nbr_choix);
    return choix;
}

void afficher(){
    char tab [1000];
    FILE* fichier;
    fichier = fopen("Trains.txt", "r");
    if (fichier != NULL){
                
        while(fgets(tab, 1000, fichier) != NULL){
        printf("%s", tab);}
    }else{
        printf("impo"); 
    }
}

/** @brief Affiche le menu au client afin qu'il choisisse ce qu'il veut faire
 *  @return int : Retourne la saisie du client
 **/
int menu_client() {
    int nbr_max_choix = 3;
    char message[] = "\nQue voulez-vous faire? Tapez 0 pour quitter :";
    printf("             **********Menu Client************\n");
    printf("   1. Consulter la liste des trains disponible.\n");
    printf("   2. soumettre une ville de depart et une ville d'arrivée\n");
    printf("   3. Consulter le prix des trains \n");
    
    return saisir(message, nbr_max_choix);
}
int main(int argc, char const *argv[]){
    struct sockaddr_in server_add = {0}; // adresse du serveur
    struct hostent *infos_server = NULL;
    int socket_client;
    int choix = -1; // temoin pour la lecture avec le buffer
    int i = 0, taille_liste_fichier;
    char buffer[1020];
    const char *hostname = "localhost"; // nom du serveur

    if (argc != 3) {
        affiche_aide();
    }

    hostname = argv[1];
    const int N_PORT = atoi(argv[2]);
    system("clear");
    socket_client = socket(AF_INET, SOCK_STREAM, 0); // initialisation du descripteur
    if (socket_client == -1)
    {
        perror("socket_client");
        exit(-1);
    }

    infos_server = gethostbyname(hostname); /* on récupère les informations du serveur */

    if (infos_server == NULL) {
        perror("infos_server");
        exit(-1);
    }

    server_add.sin_family = AF_INET;
    memcpy(&(server_add.sin_addr.s_addr), infos_server->h_addr, sizeof (u_long));
    server_add.sin_port = htons(N_PORT);

    if (connect(socket_client, (struct sockaddr *) &server_add, sizeof (server_add)) == -1) {
        perror("connect");
        exit(-1);
    }

    while (1) {
        choix = -1;
        choix = menu_client();
        if (choix == 3) {
            system("clear");
            printf("consultation des prix \n");
        } else if (choix == 2) {
            system("clear");
            funcChat(socket_client);
            
        } else if (choix == 1) {
            system("clear");
            printf("Liste des trains disponible\n");
            afficher();


        } else if (choix == 0) {
            close(socket_client);
            exit(EXIT_SUCCESS);
        }
    }


    
    close(socket_client);
    return (EXIT_SUCCESS);
}
