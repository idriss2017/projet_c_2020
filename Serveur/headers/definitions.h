#ifndef DEFINITIONS_H
#define DEFINITIONS_H


/*
* Structure pour la lecture du fichier Trains.txt
*/

typedef struct {
    int numtrain;
    char villedep[30];
    char villearv[30];
    char heuredep[6];
    char heurearv[6];
    int prix1;
    int prix2;
    char reduc[10];
} trains ;


#endif /* DEFINITIONS_H */