#ifndef FONCTIONS_H
#define FONCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>

#include "definitions.h"




void Affheure(int sockfd);
/** @brief Permet le chat du client vers le server
 *  @param int socket du client
 *  @return void
 **/
void funcChat(int sockfd);


/**  Affiche l'aide quand l'utilisateur ne sais pas comment tester
 **/
void affiche_aide();


/** @brief Méthode de traitement la demande du client
 *  @param int socket_client : Le socket (descripteur de fichier) qu'il faut servir, c'est la socket du client
 *  @return void : Pas de valeur de retour
 **/
void servir_client(int socket_client);


/** @brief Cree un processus fils qui se chargera de traiter la demande du client
 *  @param int socket_client : Le socket (descripteur de fichier) qu'il faut servir, c'est la socket du client
 *  @param int socket_server : Le socket (descripteur de fichier) qu'il faut fermer, c'est la socket du père
 *  @return void : Pas de valeur de retour
 **/
void creation_process(int socket_client, int socket_server,int choix);

void end_of_service();

/** @brief Permet la saisie des choix client en controlant les limites
 *  @param char message: message à afficher pour inviter à la saisie
 *  @param int nbr_choix: quantité maximale autorisé de choix
 *  @return int: le choix saisi par le client
 **/
int saisir(char message[], int nbr_choix);

/**
* Juste l'affichage de tous les trains disponible
*/
void afficher();

#endif /* FONCTIONS_H */